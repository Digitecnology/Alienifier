//=====================================
//Alienifier - (2008 - KevinKapo -)
//=====================================

//////////////////////////////
//Libraries
//////////////////////////////

//C specific
#include <stdio.h>
#include <stdlib.h>

//DEVIL specific
#include <IL/il.h>
#include <IL/ilu.h>

//////////////////////////////
//Macros
//////////////////////////////
#define TRUE 1
#define FALSE 0

//////////////////////////////
//Function declarations
//////////////////////////////
void ShowHelp();
int FileExists(char *filename);
int CheckForErrors();
int Alienify(char *image1, char *image2);

/* Shows the help of the program */
void ShowHelp()
{
	printf("=============================================\n");
	printf("Alienifier: Alienifies a photo of a person.\n");
	printf("=============================================\n\n");
	printf("Use: alienifier <input> <output>\n\n");
	printf("<input>   The input image to alienify.\n");
	printf("<output>  The alienified image to save.\n");
}

/* Determine if a file exists                  */
/* Returns TRUE if file exists or FALSE if not */
int FileExists(char *filename)
{
	FILE *file = fopen(filename,"r");

	if(file == NULL)
	{
		return FALSE;
	}
	else
	{
		fclose(file);
		return TRUE;
	}
}

/* Checks for errors                                  */
/* Returns TRUE if a error exists, else returns FALSE */
int CheckForErrors()
{
	if(ilGetError() == IL_NO_ERROR)
	{
		return FALSE;
	}
	else
	{
		printf("An error has ocurred.\n");
		return TRUE;
	}
}

/* Alienifies the person in image1 and save to image2 */
/* Returns TRUE if sucess of FALSE if fails           */
int Alienify(char *image1, char *image2)
{
	ILuint image;

	//Initialize DEVIL
	ilInit();
	iluInit();

	//Generate Image and check for errors
	ilGenImages(1, &image);

	if(CheckForErrors() == TRUE)
	{
		return FALSE;
	}

	//Binds the image and check for errors
	ilBindImage(image);

	if(CheckForErrors() == TRUE)
        {
                return FALSE;
        }

	//Load Image from file and check for errors
	ilLoadImage(image1);

	if(CheckForErrors() == TRUE)
        {
                return FALSE;
        }

	//Alienifies the image and check for errors
	iluAlienify();

	if(CheckForErrors() == TRUE)
        {
                return FALSE;
        }

	//Save the image to image2 and check for errors
	ilSaveImage(image2);

	if(CheckForErrors() == TRUE)
        {
                return FALSE;
        }

	//Deletes image in memory and check for errors
	ilDeleteImages(1,&image);

	if(CheckForErrors() == TRUE)
        {
                return FALSE;
        }

	return TRUE;
}

/* Main program entry */
int main(int argc, char *argv[])
{
	//If no valid arguments specified show the help
	if(argc != 3)
	{
		ShowHelp();
		return 0;
	}

	//Check if the file given is valid
	if(FileExists(argv[1]) == FALSE)
	{
		printf("Invalid filename given.\n");
		return -1;
	}

	printf("Alienifying...\n");

	//Alienifies the photo :P
	if(Alienify(argv[1],argv[2]) == TRUE)
	{
		printf("The person was alienified sucessfully :O\n");
	}
	else
	{
		printf("The alienation process has failed :(\n");
	}

	return 0;
}
